<?php

/**
 * @file srk_redirect_entity.inc
 */
class MigrateDestinationRedirect extends MigrateDestinationEntity {

  static public function getKeySchema() {
    return array(
        'rid' => array(
            'type' => 'int',
            'unsigned' => TRUE,
            'description' => 'ID of redirect',
        ),
    );
  }

  public function __construct($bundle = 'redirect', array $options = array()) {
    parent::__construct('redirect', $bundle, $options);
  }

  public static function options($language) {
    return compact('language');
  }

  public function fields($migration = NULL) {
    
    $fields = array();
    // First the core (redirect table) properties
    $fields['rid'] = t('Redirect');
    //Hash
    $fields['hash'] = t('Redirect hash value');

    $fields['type'] = t('Redirect enitity type default "@default"', array('@default' => 'redirect'));

    $fields['uid'] = t('Authored by (uid)');

    $fields['source'] = t('The source path to redirect from.');

    $fields['source_options'] = t('A serialized array of source options.');

    $fields['redirect'] = t('The destination path to redirect to.');

    $fields['redirect_options'] = t('A serialized array of redirect options.');

    $fields['language'] = t('The language this redirect is for; if blank, the alias will be used for unknown languages.');

    $fields['status_code'] = t('The HTTP status code to use for the redirect.');

    $fields['count'] = t('The number of times the redirect has been used.');

    $fields['access'] = t('The timestamp of when the redirect was last accessed.');
    
    $fields['is_new'] = t('Option :Indicates a new redirect with the specified rid should be created');
    
    // Then add in anything provided by handlers
    //$fields += migrate_handler_invoke_all('Entity', 'fields', $this->entityType, $this->bundle, $migration);

    return $fields;
  }
  
    /**
   * Delete a batch of nodes at once.
   *
   * @param $nids
   *  Array of node IDs to be deleted.
   */
  public function bulkRollback(array $rids) {
    migrate_instrument_start('redirect_delete_multiple');
    $this->prepareRollback($rids);
    redirect_delete_multiple($rids);
    $this->completeRollback($rids);
    migrate_instrument_stop('redirect_delete_multiple');
  }

  public function import(\stdClass $redirect, \stdClass $row) {
    
    // Updating previously-migrated content?
    $migration = Migration::currentMigration();
    if (isset($row->migrate_map_destid1)) {
      // Make sure is_new is off
      $redirect->is_new = FALSE;
      if (isset($redirect->rid)) {
        if ($redirect->rid != $row->migrate_map_destid1) {
          throw new MigrateException(t("Incoming rid !rid and map destination rid !destid1 don't match",
            array('!rid' => $redirect->rid, '!destid1' => $row->migrate_map_destid1)));
        }
      }
      else {
        $redirect->rid = $row->migrate_map_destid1;
      }
    }
    if ($migration->getSystemOfRecord() == Migration::DESTINATION) {
      if (!isset($redirect->rid)) {
        throw new MigrateException(t('System-of-record is DESTINATION, but no destination rid provided'));
      }
      $old_redirect = redirect_load($redirect->rid);
      if (empty($old_redirect)) {
        throw new MigrateException(t('System-of-record is DESTINATION, but redirect !rid does not exist',
                                   array('!rid' => $redirect->rid)));
      }
      if (!isset($redirect->hash)) {
        $redirect->hash = $old_redirect->hash;
      }
      if (!isset($redirect->source)) {
        $redirect->source = $old_redirect->source;
      }
      if (!isset($redirect->redirect)) {
        $redirect->redirect = $old_redirect->redirect;
      }
      if (!isset($redirect->uid)) {
        $redirect->uid = $old_redirect->uid;
      }
    }

    if (!isset($redirect->type)) {
      // Default the type to our designated destination bundle (by doing this
      // conditionally, we permit some flexibility in terms of implementing
      // migrations which can affect more than one type).
      $redirect->type = $this->bundle;
    }

    // Set some required properties.

    if ($migration->getSystemOfRecord() == Migration::SOURCE) {
      if (empty($redirect->language)) {
        $redirect->language = $this->language;
        //redirect_object_prepare overides to language one
        $language = $redirect->language;
      }

      if (isset($redirect->uid)) {
        $uid = $redirect->uid;
      }
      if (isset($redirect->source_options)) {
        $source_options = $redirect->source_options;
      }
       if (isset($redirect->redirect_options)) {
        $redirect_options = $redirect->redirect_options;
      }
      
      if(isset($redirect->status_code)) {
        $status_code = $redirect->status_code;
      }

      redirect_object_prepare($redirect);

      if (isset($redirect->hash)) {
        //setting hash here
        $redirect->hash = redirect_hash($redirect);
      }
      // No point to resetting $redirect->changed here, redirect_save() will overwrite it
      if (isset($uid)) {
        $redirect->uid = $uid;
      }
      if (isset($source_options)) {
        $redirect->source_options = $source_options;
      }
       if (isset($redirect_options)) {
        $redirect->redirect_options = $redirect_options;
      }
      
      if(isset($status_code)) {
        $redirect->status_code = $status_code;
      }     
      
      
    }
    
    if (url($redirect->source) == url($redirect->redirect)) {
      throw new MigrateException(t('You are attempting to save redirect the page to itself. This will result in an infinite loop.'));
    }

    // Invoke migration prepare handlers
    $this->prepare($redirect, $row);

    // Trying to update an existing redirect
    if ($migration->getSystemOfRecord() == Migration::DESTINATION) {
      // Incoming data overrides existing data, so only copy non-existent fields
      foreach ($old_redirect as $field => $value) {
        // An explicit NULL in the source data means to wipe to old value (i.e.,
        // don't copy it over from $old_redirect)
        if (property_exists($redirect, $field) && $redirect->$field === NULL) {
          // Ignore this field
        }
        elseif (!isset($redirect->$field)) {
          $redirect->$field = $old_redirect->$field;
        }
      }
    }

    if (isset($redirect->rid) && !(isset($redirect->is_new) && $redirect->is_new)) {
      $updating = TRUE;
    }
    else {
      $updating = FALSE;
    }

    // Make sure that if is_new is not TRUE, it is not present.
    if (isset($redirect->is_new) && empty($redirect->is_new)) {
      unset($redirect->is_new);
    }

    migrate_instrument_start('redirect_save');
    redirect_save($redirect);
    migrate_instrument_stop('redirect_save');

    if (isset($redirect->rid)) {
      if ($updating) {
        $this->numUpdated++;
      }
      else {
        $this->numCreated++;
      }
      
      $return = array($redirect->rid);
    }
    else {
      $return = FALSE;
    }

    $this->complete($redirect, $row);
    return $return;
  }
  
  /**
   * Give handlers a shot at cleaning up after an entity has been rolled back.
   *
   * @param $entity_id
   *  ID of the entity which has been deleted.
   */
  public function completeRollback($entity_id) {
    $migration = Migration::currentMigration();
    /**
     * Overriding base Entity Destination class. We need to comment out all custom field handlers since redirect entities
     * are not fieldable
     */
    // Call any general entity handlers (in particular, the builtin field handler)
    //migrate_handler_invoke_all('Entity', 'completeRollback', $entity_id);
    // Then call any entity-specific handlers
    //migrate_handler_invoke_all($this->entityType, 'completeRollback', $entity_id);
    // Then call any complete handler for this specific Migration.
    if (method_exists($migration, 'completeRollback')) {
      $migration->completeRollback($entity_id);
    }
  }
  
    public function prepare($entity, stdClass $source_row) {
    // Add source keys for debugging and identification of migrated data by hooks.
    /* TODO: Restore
    foreach ($migration->sourceKeyMap() as $field_name => $key_name) {
      $keys[$key_name] = $source_row->$field_name;
    }
    */
    $migration = Migration::currentMigration();
    $entity->migrate = array(
//      'source_keys' => $keys,
      'machineName' => $migration->getMachineName(),
    );
    
    /**
     * @see completeRollback($entity_id) comment
     */

    // Call any general entity handlers (in particular, the builtin field handler)
    //migrate_handler_invoke_all('Entity', 'prepare', $entity, $source_row);
    // Then call any entity-specific handlers
    //migrate_handler_invoke_all($this->entityType, 'prepare', $entity, $source_row);

    // Apply defaults, removing empty fields from the entity object.
    //$form = $form_state = array();
    //_field_invoke_default('submit', $this->entityType, $entity, $form, $form_state);

    // Then call any prepare handler for this specific Migration.
    if (method_exists($migration, 'prepare')) {
      $migration->prepare($entity, $source_row);
    }
  }
  
    /**
   * Give handlers a shot at modifying the object (or taking additional action)
   * after saving it.
   *
   * @param $object
   *  Entity object to build. This is the complete object after saving.
   * @param $source_row
   *  Raw source data object - passed through to complete handlers.
   */
  public function complete($entity, stdClass $source_row) {
    
     /**
     * @see completeRollback($entity_id) comment
     */
    
    // Call any general entity handlers (in particular, the builtin field handler)
    //migrate_handler_invoke_all('Entity', 'complete', $entity, $source_row);
    // Then call any entity-specific handlers
    //migrate_handler_invoke_all($this->entityType, 'complete', $entity, $source_row);
    // Then call any complete handler for this specific Migration.
    $migration = Migration::currentMigration();
    if (method_exists($migration, 'complete')) {
      try {
        $migration->complete($entity, $source_row);
      }
      catch (Exception $e) {
        // If we catch any errors here, save the messages without letting
        // the exception prevent the saving of the entity being recorded.
        $migration->saveMessage($e->getMessage());
      }
    }
  }
  
    /**
   * Give handlers a shot at cleaning up before an entity has been rolled back.
   *
   * @param $entity_id
   *  ID of the entity about to be deleted..
   */
  public function prepareRollback($entity_id) {
    $migration = Migration::currentMigration();

    /**
     * @see completeRollback($entity_id) comment
     */    
    
    // Call any general entity handlers (in particular, the builtin field handler)
    //migrate_handler_invoke_all('Entity', 'prepareRollback', $entity_id);
    // Then call any entity-specific handlers
    //migrate_handler_invoke_all($this->entityType, 'prepareRollback', $entity_id);
    // Then call any prepare handler for this specific Migration.
    if (method_exists($migration, 'prepareRollback')) {
      $migration->prepareRollback($entity_id);
    }
  }

}
